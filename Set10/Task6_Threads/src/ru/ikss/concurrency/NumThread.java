package ru.ikss.concurrency;

public class NumThread extends Thread {

    private int ID;
    private Object lock;
    public static long count = 0;

    public NumThread(int id, Object lock) {
        this.ID = id;
        this.lock = lock;
    }

    public synchronized void lock() throws InterruptedException {
        count++;

        lock.wait();
        System.out.println(this.ID);
        if (count % 4 == 0) {
            lock.notifyAll();
            System.out.println(this.ID);
        }
        System.out.println(this.ID);

    }

    @Override
    public void run() {
        while (true) {
            try {
                this.lock();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }
}
