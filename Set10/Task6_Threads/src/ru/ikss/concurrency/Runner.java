package ru.ikss.concurrency;

public class Runner {

    public static void main(String[] args) throws InterruptedException {

        Object lock = new Object();
        for (int i = 1; i < 6; i++) {
            Thread thread = new NumThread(i, lock);
            thread.setPriority(Thread.MAX_PRIORITY - i);
            thread.start();
        }
    }
}
