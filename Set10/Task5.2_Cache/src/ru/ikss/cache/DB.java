package ru.ikss.cache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DB {

    public static Connection getConnection() throws SQLException {
        try {
            Connection conn = null;
            String dbHost = "172.16.4.0";
            String dbName = "SES";
            String dbUser = "sa";
            String dbPassword = "mssql";
            String jdbcUrl = "jdbc:sqlserver://" + dbHost + ";databasename=" + dbName;
            String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            Class.forName(driverName);
            Properties properties = new java.util.Properties();

            properties.setProperty("user", dbUser);
            properties.setProperty("password", dbPassword);
            properties.setProperty("integratedSecurity", "false");

            properties.setProperty("autoReconnect", "true");
            DriverManager.setLoginTimeout(3);
            conn = DriverManager.getConnection(jdbcUrl, properties);

            if (conn == null) {
                throw new SQLException("Не удалось подключиться к БД");
            }
            return conn;
        }

        catch (ClassNotFoundException ex) {
            throw new SQLException("MSSQL driver not found");
        }
    }
}
