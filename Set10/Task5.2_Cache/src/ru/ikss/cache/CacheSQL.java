package ru.ikss.cache;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.sql.rowset.CachedRowSet;

class CachedElement {

    private Date date;
    private CachedRowSet context;

    public CachedElement(Date date, CachedRowSet context) {
        this.date = date;
        this.context = context;
    }

    public Date getDate() {
        return date;
    }

    public CachedRowSet getContext() {
        return context;
    }
}

public enum CacheSQL {
    INSTANCE;

    static final long CACHE_SIZE = 1000L;
    static final int CACHE_TIME = 3600;

    HashMap<String, CachedElement> cache;

    private CacheSQL() {
        cache = new HashMap<String, CachedElement>();
    }

    public HashMap<String, CachedElement> getCache() {
        return cache;
    }

    public void addToCache(String sql, CachedRowSet set) {
        if ((cache.size() + 1 > CACHE_SIZE) && (cache.get(sql) == null)) {
            removeOldest();
        }
        cache.put(sql, new CachedElement(Calendar.getInstance().getTime(), set));
    }

    private void removeOldest() {
        Date date = Calendar.getInstance().getTime();
        String key = null;
        for (Map.Entry<String, CachedElement> e : cache.entrySet()) {
            Date dateDel = e.getValue().getDate();
            if (dateDel.compareTo(date) == -1) {
                date = dateDel;
                key = e.getKey();
            }
        }
        if (key != null) {
            cache.remove(key);
        }
    }

    public CachedElement getFromCache(String sql) {
        CachedElement element = cache.get(sql);
        if (element != null) {
            long timeDiff = Calendar.getInstance().getTime().getTime() - element.getDate().getTime();
            if (TimeUnit.MILLISECONDS.toSeconds(timeDiff) > CACHE_TIME) {
                cache.remove(sql);
                element = null;
            }
        }
        return element;
    }
}
