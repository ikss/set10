package ru.ikss.cache;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestRun {

    public static void main(String[] args) {
        try (Connection conn = DB.getConnection()) {
            SQLStatement sql = new SQLStatement(conn);
            ResultSet rs1 = sql.queryCached("select top 10 * from chequehead");
            while (rs1.next()) {
                System.out.println(rs1.getRow() + "\t" + rs1.getInt("Id"));
            }
            // rs1.beforeFirst();
            ResultSet rs2 = sql.queryCached("select top 10 * from chequehead");
            while (rs2.next()) {
                System.out.println(rs2.getRow() + "\t" + rs2.getInt("Id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
