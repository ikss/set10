package ru.ikss.cache;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;

public class SQLStatement {

    private Connection conn;
    private CacheSQL cacheSQL;

    public SQLStatement(Connection conn) {
        this.conn = conn;
    }

    public ResultSet query(String sql) throws SQLException {
        CachedRowSet crs = new CachedRowSetImpl();
        Statement statement = conn.createStatement();
        crs.populate(statement.executeQuery(sql));
        cacheSQL.INSTANCE.addToCache(sql, crs);
        return crs;
    }

    public ResultSet queryCached(String sql) throws SQLException {
        CachedElement element = cacheSQL.INSTANCE.getFromCache(sql);
        if (element != null)
            return element.getContext();
        return query(sql);
    }
}
