package ru.ikss.fileAnalyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FileAnalyzer {

    File file;

    public FileAnalyzer(String path) throws IOException {
        if (path.equals(""))
            throw new IOException("Введена пустая строка");
        File file = new File(path);
        if (!file.exists())
            throw new IOException("Указанного файла не существует");
        else if (file.isDirectory())
            throw new IOException("Указан путь к каталогу");
        this.file = file;
    }

    public void getFileSize() {
        System.out.printf("Размер файла: %d байт\n", file.length());
    }

    public void getSymbolsCount() {
        System.out.println("Количество вхождений символа в файл:");
        Map<String, Integer> symbolsMap = new HashMap<String, Integer>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            char[] buffer = new char[(int) file.length()];
            if (br.read(buffer) != -1)
                for (char c : buffer) {
                    symbolsMap.put("" + c, symbolsMap.getOrDefault("" + c, 0) + 1);
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Entry<String, Integer> e : symbolsMap.entrySet())
            System.out.println("\"" + e.getKey() + "\"\t" + e.getValue());
        System.out.println();
    }

    public void getCounts() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            Set<String> wordsMap = new HashSet<String>();
            Map<Long, Integer> numbersMap = new HashMap<Long, Integer>();
            long incCount = 0L;
            while ((line = br.readLine()) != null) {
                for (String s : line.split("\\s+")) {
                    if (s.matches("[A-Za-z]{7}")) {
                        wordsMap.add(s);
                    } else if (s.matches("[1-9]+")) {
                        long value = Long.parseLong(s);
                        if (value == (incCount + 1)) {
                            incCount++;
                        }
                        numbersMap.put(value, numbersMap.getOrDefault(value, 0) + 1);
                    }
                }
            }
            System.out.println("Сумма нарастающего счетчика равна: " + (incCount + 1) * incCount / 2);
            System.out.println("Количество разных чисел в файле:");
            for (Entry<Long, Integer> e : numbersMap.entrySet()) {
                System.out.println(e.getKey() + "\t" + e.getValue());
            }
            System.out.println();
            System.out.println("Уникальные слова из семи символов:");
            wordsMap.forEach(System.out::println);
            System.out.println();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();

    }
}
