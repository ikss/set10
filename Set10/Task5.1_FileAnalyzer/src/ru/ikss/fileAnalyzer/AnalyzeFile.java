package ru.ikss.fileAnalyzer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AnalyzeFile {

    public static void main(String... argv) {
        System.out.println("Введите имя файла:");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            FileAnalyzer analyzer = new FileAnalyzer(reader.readLine().trim());
            analyzer.getFileSize();
            analyzer.getSymbolsCount();
            analyzer.getCounts();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
