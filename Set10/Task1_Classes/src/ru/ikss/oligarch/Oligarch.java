package ru.ikss.oligarch;

import java.util.ArrayList;
import java.util.List;

import ru.ikss.printer.Printer;
import ru.ikss.roulette.Roulette;

class Casino {

    List<Roulette> roulettes = new ArrayList<Roulette>();

    public Casino(Printer printer) {
        for (int i = 0; i < 5; i++) {
            roulettes.add(new Roulette(printer));
            printer.print("Copy");
        }
    }

}

public final class Oligarch {

    static Printer printer = new Printer();

    static List<Casino> casinos = new ArrayList<Casino>();

    public static void main(String[] argv) {
        casinos.add(new Casino(printer));
        casinos.add(new Casino(printer));
    }
}
