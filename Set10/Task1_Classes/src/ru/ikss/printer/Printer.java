package ru.ikss.printer;

public final class Printer {

    private String prev;

    public void print(Integer a) {
        prev = Integer.toString(a);
        System.out.println(prev);
    }

    public void print(String s) {
        if (s.equals("Copy"))
            System.out.println("Copy " + prev);
    }

}
