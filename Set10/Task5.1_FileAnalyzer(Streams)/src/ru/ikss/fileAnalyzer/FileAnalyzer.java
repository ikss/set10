package ru.ikss.fileAnalyzer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class FileAnalyzer {

    Path path = null;

    public FileAnalyzer(String fileName) throws IOException {
        if (fileName.equals(""))
            throw new IOException("Введена пустая строка");
        this.path = Paths.get(fileName);
        if (!Files.exists(path))
            throw new IOException("Указанного файла не существует");
        else if (Files.isDirectory(path))
            throw new IOException("Указан путь к каталогу");
    }

    public void getSymbolsCount() throws IOException {
        System.out.println("Количество вхождений символа в файл:");
        byte[] byteArr = Files.readAllBytes(path);
        Map<String, Integer> symbolsMap = new HashMap<String, Integer>();
        for (byte b : byteArr)
            symbolsMap.put("" + (char) b, symbolsMap.getOrDefault("" + (char) b, 0) + 1);
        for (Entry<String, Integer> e : symbolsMap.entrySet())
            System.out.println("\"" + e.getKey() + "\"\t" + e.getValue());
        System.out.println();
    }

    public void getSevenSymbolsWord() throws IOException {
        System.out.println("Уникальные слова из семи символов:");
        Files.lines(path)
            .flatMap(s -> Stream.of(s.split("\\s+")))
            .filter(s -> s.matches("[a-zA-Z]{7}"))
            .distinct()
            .forEach(System.out::println);
        System.out.println();
    }

    public void getNumbersCount() throws IOException {
        System.out.println("Количество разных чисел в файле:");
        Files.lines(path)
            .flatMap(s -> Stream.of(s.split("\\s+")))
            .filter(s -> s.matches("[0-9]+"))
            .map(Long::valueOf)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .forEach((number, count) -> System.out.println(number + "\t" + count));
        System.out.println();
    }

    public void getIncrementCount() throws IOException {
        long value = 0L;
        List<Long> incCounter = Files.lines(path)
            .flatMap(s -> Stream.of(s.split("\\s+")))
            .filter(s -> s.matches("[0-9]+"))
            .map(Long::valueOf)
            .collect(Collectors.toList());
        for (Long element : incCounter) {
            if (element.longValue() == value + 1)
                value++;
        }
        System.out.println("Сумма нарастающего счетчика: " + (value + 1) * value / 2);
        System.out.println();
    }

    public void getFileSize() throws IOException {
        System.out.printf("Размер файла: %d байт\n\n", Files.size(path));
    }
}
