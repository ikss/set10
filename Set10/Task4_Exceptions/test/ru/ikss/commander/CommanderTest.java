package ru.ikss.commander;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.junit.Test;

public class CommanderTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void test() {
        System.setOut(new PrintStream(outContent));
        CommandProcessor processor = new CommandProcessor();
        String[] param = new String[] {"5", "6"};
        processor.process("SUM", Arrays.asList(param));
        assertEquals("Sum = 11.0\r\n", outContent.toString());
        outContent.reset();
        param = new String[] {"ard"};
        processor.process("CD", Arrays.asList(param));
        assertEquals("ERROR: \"ard\" is not a directory\r\n", outContent.toString());
    }
}
