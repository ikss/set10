package ru.ikss.commander;

import java.util.List;

public class SUMCommand implements ICommand {

    final static int ARGS_COUNT = 2;

    List<String> args;

    public SUMCommand(List<String> args) throws CommanderExeception {
        if (args.size() != ARGS_COUNT) {
            throw new CommanderExeception("Wrong number of arguments");
        }
        this.args = args;
    }

    @Override
    public void process() throws CommanderExeception {
        try {
            Double a = Double.parseDouble(args.get(0));
            Double b = Double.parseDouble(args.get(1));
            System.out.println("Sum = " + (a + b));
        } catch (NumberFormatException e) {
            throw new CommanderExeception("Wrong argument type " + e.getMessage());
        }
    }

}
