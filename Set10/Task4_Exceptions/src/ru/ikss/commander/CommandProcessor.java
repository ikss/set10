package ru.ikss.commander;

import java.util.List;

public class CommandProcessor {

    public void process(String cmd, List<String> args) {
        try {
            ICommand command = null;
            switch (cmd) {
                case ("CD"): {
                    command = new CDCommand(args);
                    break;
                }
                case ("DIR"): {
                    command = new DIRCommand(args);
                    break;
                }
                case ("SUM"): {
                    command = new SUMCommand(args);
                    break;
                }
                default: {
                    throw new CommanderExeception("Unknown command \"" + cmd + "\"");
                }
            }
            if (command != null) {
                command.process();
            }
        } catch (CommanderExeception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
