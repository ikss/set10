package ru.ikss.commander;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

public class DIRCommand implements ICommand {

    final static int ARGS_COUNT = 0;

    List<String> args;

    public DIRCommand(List<String> args) throws CommanderExeception {
        if (args.size() != ARGS_COUNT) {
            throw new CommanderExeception("Wrong number of arguments");
        }
        this.args = args;
    }

    @Override
    public void process() throws CommanderExeception {
        System.out.println("Содержимое папки " + Parameters.curFolder);
        Stream.of(new File(Parameters.curFolder)
            .listFiles())
            .map(s -> s.getPath().substring(Parameters.curFolder.length()))
            .forEach(System.out::println);
    }

}
