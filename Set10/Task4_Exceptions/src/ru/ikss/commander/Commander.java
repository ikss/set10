package ru.ikss.commander;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Обработчик комманд */
public class Commander {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str = null;
        Parameters.curFolder = System.getProperty("user.dir");
        System.out.println("Commander v.1");
        System.out.println(Parameters.curFolder + ">");
        CommandProcessor processor = new CommandProcessor();
        try {
            while ((str = reader.readLine()) != null) {
                if (str.equalsIgnoreCase("Exit")) {
                    System.out.println("Exit from commander");
                    break;
                }
                String[] cmdArr = str.trim().split("[ ]+");
                String cmd = cmdArr[0].toUpperCase();
                List<String> cmdArgs = Stream.of(cmdArr).skip(1).collect(Collectors.toList());
                processor.process(cmd, cmdArgs);
                System.out.println();
                System.out.println(Parameters.curFolder + ">");
            }
        } catch (IOException e) {
            System.out.println("Catch IOException on read" + e.getMessage());
        }
        try {
            reader.close();
        } catch (IOException e) {
            System.out.println("Catch IOException on close" + e.getMessage());
        }
    }
}
