package ru.ikss.commander;

public class CommanderExeception extends Exception {

    private static final long serialVersionUID = 1L;

    public CommanderExeception() {
        super();
    }

    public CommanderExeception(String message) {
        super(message);
    }

    public CommanderExeception(Throwable cause) {
        super(cause);
    }

    public CommanderExeception(String message, Throwable cause) {
        super(message, cause);
    }

}
