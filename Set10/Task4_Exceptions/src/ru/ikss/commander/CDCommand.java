package ru.ikss.commander;

import java.io.File;
import java.util.List;

public class CDCommand implements ICommand {

    final static int ARGS_COUNT = 1;

    List<String> args;

    public CDCommand(List<String> args) throws CommanderExeception {
        if (args.size() != ARGS_COUNT) {
            throw new CommanderExeception("Wrong number of arguments");
        }
        this.args = args;
    }

    @Override
    public void process() throws CommanderExeception {
        File f = new File(args.get(0));
        if (!f.isDirectory()) {
            throw new CommanderExeception("\"" + args.get(0) + "\"" + " is not a directory");
        }
        Parameters.curFolder = args.get(0);
    }
}
