package ru.ikss.commander;

/** Интерфейс команды */
public interface ICommand {

    /** Выполнение команды команды */
    public void process() throws CommanderExeception;

}
