package ru.ikss.geometry;

public class Point3D extends Point2D {

    protected double z;

    public Point3D(double x, double y, double z) {
        super(x, y);
        this.z = z;
    }

    public double distance(double x, double y, double z) {
        double dx = x - this.getX();
        double dy = y - this.getY();
        double dz = z - this.getZ();
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    public double distance(Point3D point) {
        double dx = point.getX() - this.getX();
        double dy = point.getY() - this.getY();
        double dz = point.getZ() - this.getZ();
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    public double getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
