package ru.ikss.geometry;

public class Point2D extends Point1D {

    protected double y;

    public Point2D(double x, double y) {
        super(x);
        this.y = y;
    }

    double distance(double x, double y) {
        double dx = x - this.getX();
        double dy = y - this.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }

    public double distance(Point2D point) {
        double dx = point.getX() - this.getX();
        double dy = point.getY() - this.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
