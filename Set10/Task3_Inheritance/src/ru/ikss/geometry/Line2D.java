package ru.ikss.geometry;

public class Line2D {

    protected double x1;
    protected double x2;
    protected double y1;
    protected double y2;

    public Line2D(double x1, double y1, double x2, double y2) {
        this(new Point2D(x1, y1), new Point2D(x2, y2));
    }

    public Line2D(Point2D point1, Point2D point2) {
        this.x1 = point1.getX();
        this.y1 = point1.getY();
        this.x2 = point2.getX();
        this.y2 = point2.getY();
    }

    public double angleTo(Line2D line) {
        double a1 = y1 - y2;
        double a2 = line.getY1() - line.getY2();
        double b1 = x1 - x2;
        double b2 = line.getX1() - line.getX2();
        double result = 0d;
        try {
            result = Math.toDegrees(Math.atan((a1 * b2 - a2 * b1) / (a1 * a2 + b1 * b2)));
        } catch (ArithmeticException e) {
            System.out.println(e.getClass() + " " + e.getStackTrace());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Line2D))
            return false;
        Line2D test = (Line2D) obj;
        return (test.getX1() == this.getX1()) && (test.getX2() == this.getX2()) && (test.getY1() == this.getY1()) && (test.getY2() == this.getY2());
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public double getY2() {
        return y2;
    }

    public void setY2(double y2) {
        this.y2 = y2;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }
}
