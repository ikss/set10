package ru.ikss.geometry;

public class Line3D extends Line2D {

    protected double z1;
    protected double z2;

    public Line3D(double x1, double y1, double z1, double x2, double y2, double z2) {
        this(new Point3D(x1, y1, z1), new Point3D(x2, y2, z2));
    }

    public Line3D(Point3D point1, Point3D point2) {
        super(point1.getX(), point1.getY(), point2.getX(), point2.getY());
        this.z1 = point1.getZ();
        this.z2 = point2.getZ();
    }

    public double angleTo(Line3D line) {
        double a1 = y1 - y2;
        double a2 = line.getY1() - line.getY2();
        double b1 = x1 - x2;
        double b2 = line.getX1() - line.getX2();
        double c1 = z1 - z2;
        double c2 = line.getZ1() - line.getZ2();
        double result = 0d;
        try {
            result = Math.toDegrees(Math
                .acos(Math.abs((a1 * a2 + b1 * b2 + c1 * c2) / (Math.sqrt(a1 * a1 + b1 * b1 + c1 * c1) * Math.sqrt(a2 * a2 + b2 * b2 + c2 * c2)))));
        } catch (ArithmeticException e) {
            System.out.println(e.getClass() + " " + e.getStackTrace());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Line3D))
            return false;
        Line3D test = (Line3D) obj;
        return (test.getX1() == this.getX1()) && (test.getX2() == this.getX2()) && (test.getY1() == this.getY1()) && (test.getY2() == this.getY2()) &&
            (test.getZ1() == this.getZ1()) && (test.getZ2() == this.getZ2());
    }

    public double getZ1() {
        return z1;
    }

    public void setZ1(double z1) {
        this.z1 = z1;
    }

    public double getZ2() {
        return z2;
    }

    public void setZ2(double z2) {
        this.z2 = z2;
    }

}
