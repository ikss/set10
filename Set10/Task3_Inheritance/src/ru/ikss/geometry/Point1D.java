package ru.ikss.geometry;

public class Point1D {

    protected double x;

    public Point1D(double x) {
        this.x = x;
    }

    public double distance(double x) {
        double dx = x - this.getX();
        return Math.abs(dx);
    }

    public double distance(Point1D point) {
        double dx = point.getX() - this.getX();
        return Math.abs(dx);
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

}
