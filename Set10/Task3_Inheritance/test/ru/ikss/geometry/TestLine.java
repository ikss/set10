package ru.ikss.geometry;

import org.junit.Assert;
import org.junit.Test;

public class TestLine {

    @Test
    public void lineEquivalence() {
        Line2D l1 = new Line2D(1d, -2.5d, 4d, 15d);
        Line2D l2 = new Line2D(new Point2D(1d, -2.5d), new Point2D(4d, 15d));
        Assert.assertEquals(l1, l2);
        Line3D l3 = new Line3D(-18d, 0, 1d, 15d, -23.5d, 36d);
        Line3D l4 = new Line3D(new Point3D(-18d, 0, 1d), new Point3D(15d, -23.5d, 36d));
        Assert.assertEquals(l3, l4);
    }

    @Test
    public void lineAngle() {
        Line2D l1 = new Line2D(-2.3d, 15d, 0, -6d);
        Line2D l2 = new Line2D(18.4d, -2d, 5, 4.5d);
        Assert.assertEquals("���� ����� 2D �������", -57.8728137381d, l1.angleTo(l2), 1E-10);
        Line3D l3 = new Line3D(-11.2d, 18d, 3d, 1d, 18d, 92d);
        Line3D l4 = new Line3D(8.3d, 2d, -5d, 37d, -11.1d, -3d);
        Assert.assertEquals("���� ����� 3D �������", 79.2816658794d, l4.angleTo(l3), 1E-10);
    }

}
