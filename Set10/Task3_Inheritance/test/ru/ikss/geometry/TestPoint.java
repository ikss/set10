package ru.ikss.geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPoint {

    @Test
    public void pointDistance() {
        Point1D p1 = new Point1D(15.6d);
        Point1D p2 = new Point1D(-13.3d);
        Point2D p3 = new Point2D(-3d, 4);
        Point2D p4 = new Point2D(0, 0);
        assertEquals("���������� ����� ������� 1D", 28.9d, p1.distance(p2), 1E-10);
        assertEquals("���������� ����� ������� 2D", 5d, p4.distance(p3), 1E-10);

    }

}
